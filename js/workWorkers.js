const overflowSection = document.getElementById('overflow-section-wrap');
const navSection = document.getElementById('nav-section');
const arrowNavLeft = document.getElementById('arrow-left');
const workSvg = document.getElementById('work-svg');
const workersSvg = document.getElementById('workers-svg');
const arrowNavRight = document.getElementById('arrow-right');
const workSection = document.getElementById('work');
const workArrowBack = document.getElementById('work-arrow-back');
const workerSection = document.getElementById('workers');
const workersArrowBack = document.getElementById('workers-arrow-back');
const workersContent = document.getElementById('workers-content');
const workersImages = document.getElementById('workers-images');
const workers = document.querySelectorAll('.workers__worker');
let curWork = 1;
const numSections = document.getElementsByClassName('work__section').length;
const workPrefix = 'work-';
const transitionEl = document.getElementById('transition');
const transTime = 1250;
const workPopup = document.getElementsByClassName('work__popup__content');
const rules = document.querySelectorAll('.rules');
let count = 0;

window.onload = () => {
  overflowSection &&
    overflowSection.scrollTo({ left: navSection.getBoundingClientRect().left });
  if (numSections > 0) {
    handleWorkListeners();
  }
  // handleWorkersListener();
  handlePortfolioListener();
};

window.onresize = () => {
  overflowSection.scroll({ left: navSection.getBoundingClientRect().left });
};

const preventDefault = (e) => {
  e.preventDefault();
};
const disableScroll = () => {
  document.addEventListener('wheel', preventDefault, { passive: false });
  document.addEventListener('touchmove', preventDefault, { passive: false });
};

arrowNavLeft
  ? (arrowNavLeft.onclick = () => {
      navigateLeft();
    })
  : null;

workSvg
  ? (workSvg.onclick = () => {
      navigateLeft();
    })
  : null;

const navigateLeft = () => {
  document.removeEventListener('wheel', wheelBehavior, { passive: false });
  workSection.classList.add('in-view');
  navSection.classList.add('slide-right');
  disableScroll();
};

arrowNavRight
  ? (arrowNavRight.onclick = () => {
      navigateRight();
    })
  : null;

workersSvg
  ? (workersSvg.onclick = () => {
      navigateRight();
    })
  : null;

const navigateRight = () => {
  document.removeEventListener('wheel', wheelBehavior, { passive: false });
  workerSection.classList.add('in-view');
  navSection.classList.add('slide-left');
  document.querySelector(
    '.overflow-section > .section__arrow--bottom'
  ).style.display = 'none';
  disableScroll();
};

workArrowBack
  ? (workArrowBack.onclick = () => {
      workSection.classList.remove('in-view');
      navSection.classList.remove('slide-right');
      document.addEventListener('wheel', wheelBehavior, { passive: false });
    })
  : null;

workersArrowBack
  ? (workersArrowBack.onclick = () => {
      workerSection.classList.remove('in-view');
      navSection.classList.remove('slide-left');
      setTimeout(() => {
        document.querySelector(
          '.overflow-section > .section__arrow--bottom'
        ).style.display = 'block';
      }, 600);
      document.addEventListener('wheel', wheelBehavior, { passive: false });
    })
  : null;

const scrollTransition = () => {
  transitionEl.classList.add('in-progress');
  transitionEl.ontransitionend = () => {
    transitionEl.classList.remove('in-progress');
  };
};

// FOR SCROLLING
// const workPagination = () => {
//   let workOffset = document.getElementById(workPrefix + curWork).offsetTop;
//   overflowSection.scroll({ top: workOffset, behavior: 'smooth' });
//   setTimeout(() => {
//     scrolling = false;
//   }, transTime);
// };

// FOR TRANSITION
const workPagination = (direction) => {
  const prevEl = document.getElementById(workPrefix + (curWork - 1));
  const curEl = document.getElementById(workPrefix + curWork);
  if (direction === 'down') {
    if (curWork === numSections) {
      document.querySelector('.work__bottom__arrow').style.display = 'none';
    }
    curWork - 1 !== numSections && ruleAnimation();
    curWork - 1 === 1 && prevEl.classList.add('zoom');
    setTimeout(() => {
      prevEl.classList.add('scrolled');
    }, 200);
    setTimeout(() => {
      curEl.classList.add('zoom');
    }, 1500);
  }
  if (direction === 'up') {
    console.log(curWork, numSections);
    if (curWork < numSections) {
      document.querySelector('.work__bottom__arrow').style.display = 'block';
    }
    curWork + 1 !== 1 && ruleAnimation();
    setTimeout(() => {
      curEl.classList.remove('scrolled');
      setTimeout(() => {
        curEl.classList.remove('zoom');
      }, 1500);
    }, 200);
  }
  setTimeout(() => {
    scrolling = false;
  }, transTime);
};

const scrollDown = () => {
  if (curWork === numSections) {
    setTimeout(() => {
      scrolling = false;
    }, transTime);
    return;
  }
  btnAnimationDown();
  curWork++;
  workPagination('down');
};

const scrollUp = () => {
  if (curWork === 1) {
    setTimeout(() => {
      scrolling = false;
    }, transTime);
    return;
  }
  curWork--;
  btnAnimationUp();
  workPagination('up');
};

const ruleAnimation = () => {
  curWork >= 1 &&
    curWork <= numSections &&
    rules.forEach((element) => {
      element.classList.add('animate');
      setTimeout(() => {
        element.classList.remove('animate');
        element.classList.add('re-animate');
        setTimeout(() => {
          element.classList.remove('re-animate');
        }, 800);
      }, 800);
    });
};

const btnAnimationDown = () => {
  const toggleBtn = document
    .getElementById(workPrefix + curWork)
    .querySelector('.work-toggle');
  curWork >= 1 && toggleBtn.classList.add('animate');
  setTimeout(() => {
    document
      .getElementById(workPrefix + curWork)
      .querySelector('.work-toggle')
      .classList.remove('animate');
  }, 1500);
};

const btnAnimationUp = () => {
  const toggleBtn = document
    .getElementById(workPrefix + curWork)
    .querySelector('.work-toggle');
  curWork >= 1 &&
    document
      .getElementById(workPrefix + (curWork + 1))
      .querySelector('.work-toggle')
      .classList.add('animate');
  setTimeout(() => {
    toggleBtn.classList.remove('animate');
    document
      .getElementById(workPrefix + (curWork + 1))
      .querySelector('.work-toggle')
      .classList.add('animate');
  }, 1500);
};

const workScroll = (e) => {
  e.preventDefault();
  e.stopPropagation();
  if (!scrolling) {
    scrolling = true;
    const scrollDirection = Math.sign(e.deltaY);
    // ruleAnimation();
    if (scrollDirection > 0) {
      scrollDown();
    } else {
      scrollUp();
    }
  }
};

const bottomArrowClick = () => {
  document.querySelector('.work__bottom__arrow') &&
    document
      .querySelector('.work__bottom__arrow')
      .addEventListener('click', () => {
        scrollDown();
      });
};

bottomArrowClick();

workSection ? workSection.addEventListener('wheel', workScroll) : null;
workSection ? workSection.addEventListener('touch', workScroll) : null;

const workToggle = document.querySelectorAll('div.work-toggle');
const closeWorkToggle = document.querySelectorAll('div.close-work');

const scrollWork = (e) => {
  e.stopPropagation();
};

const handleWorkListeners = () => {
  for (let i = 0; i < numSections; i++) {
    const popupSection = document.getElementById(workToggle[i].dataset.work);

    // TOGGLE OPEN WORK POPUP
    workToggle[i].addEventListener('click', () => {
      popupSection.addEventListener('wheel', scrollWork, { passive: true });
      popupSection.addEventListener('touch', scrollWork, { passive: true });
      handleWorkPopupScrollListener();
      scrollTransition();
      document.getElementsByTagName('body')[0].classList.add('no-scroll');
      document.querySelector('.rules--top').style.display = 'none';
      document.querySelector('.rules--bottom').style.display = 'none';
      document.querySelector('.work__bottom__arrow').style.display = 'none';
      setTimeout(() => {
        popupSection.classList.add('active');
        workSection.removeEventListener('wheel', workScroll);
        workSection.addEventListener('wheel', preventDefault, {
          passive: false,
        });
      }, 800);
    });

    // CLOSE WORK POPUP
    closeWorkToggle[i].addEventListener('click', () => {
      scrollTransition();
      document.querySelector('.work__bottom__arrow').style.display = 'block';
      document.querySelector('.rules--top').style.display = 'flex';
      document.querySelector('.rules--bottom').style.display = 'flex';
      document.getElementsByTagName('body')[0].classList.remove('no-scroll');
      setTimeout(() => {
        popupSection.classList.remove('active');
        workSection.addEventListener('wheel', workScroll);
        workSection.removeEventListener('wheel', preventDefault);
      }, 800);
    });

    // ADD Z-INDEX TO WORK SECTIONS
    document.getElementById(workPrefix + (i + 1)).style.zIndex =
      numSections - i;
  }
};

const portfolioToggle = document.querySelectorAll('div.portfolio-toggle');
const closePortfolioToggle = document.querySelectorAll('div.close-portfolio');

const handlePortfolioListener = () => {
  for (let w = 0; w < portfolioToggle.length; w++) {
    const portfolioPopup = document.getElementById(
      portfolioToggle[w].dataset.portfolio
    );

    portfolioToggle[w].addEventListener('click', () => {
      scrollTransition();
      handleWorkPopupScrollListener();
      setTimeout(() => {
        portfolioPopup.classList.add('active');
      }, 800);
    });

    closePortfolioToggle[w].addEventListener('click', (e) => {
      scrollTransition();
      setTimeout(() => {
        portfolioPopup.classList.remove('active');
      }, 800);
    });
  }
};

const handleWorkPopupScrollListener = () => {
  const popup = document.querySelectorAll('.work__popup__content');
  const windowHeight = window.innerHeight;

  popup &&
    popup.forEach((element) => {
      const children = element.childNodes;
      const childEls = Array.from(children).filter(
        (el) => el.nodeName !== '#text'
      );
      if (childEls.length > 0) {
        for (let count = 0; count < 3; count++) {
          childEls[count] !== undefined &&
            childEls[count].classList.add('visible');
        }
      }

      element.addEventListener('wheel', () => {
        for (let c = 0; c < children.length; c++) {
          const child = children[c];
          if (child.nodeName !== '#text') {
            const childPos = child.getBoundingClientRect();
            if (childPos.top < windowHeight) {
              child.classList.add('visible');
            } else {
              child.classList.remove('visible');
            }
          }
        }
      });

      element.addEventListener('touchmove', () => {
        for (let c = 0; c < children.length; c++) {
          const child = children[c];
          if (child.nodeName !== '#text') {
            const childPos = child.getBoundingClientRect();
            if (childPos.top < windowHeight) {
              child.classList.add('visible');
            } else {
              child.classList.remove('visible');
            }
          }
        }
      });
    });
};
let currentSlide = 0;

function workerImgSlider(worker, curSlide) {
  const imgSlides = worker.querySelectorAll('div.workers__img-wrap > img');

  curSlide >= imgSlides.length && (curSlide = 0);
  curSlide < 0 && (curSlide = imgSlides.length - 1);

  imgSlides[currentSlide].classList.toggle('active-img');
  imgSlides[curSlide].classList.toggle('active-img');

  currentSlide = curSlide;
}

workers.forEach((worker) =>
  worker
    .querySelector('.workers__img-frame .nav-links--prev')
    .addEventListener('click', () => {
      workerImgSlider(worker, currentSlide - 1);
    })
);
workers.forEach((worker) =>
  worker
    .querySelector('.workers__img-frame .nav-links--next')
    .addEventListener('click', () => {
      workerImgSlider(worker, currentSlide + 1);
    })
);

// const workerImgSlider = (event) => {
//   const workerImgs = event.target.querySelectorAll('.workers__img-wrap img');
//   for (let n = 0; n < workerImgs.length - 1; n++) {
//     workerImgs.forEach((img) => {
//       n === 0
//         ? (img.style.transform =
//             'translateX(-' + workerImgs[n].clientWidth * (n + 1) + 'px)')
//         : setTimeout(() => {
//             img.style.transform =
//               'translateX(-' + workerImgs[n].clientWidth * (n + 1) + 'px)';
//           }, 2000);
//     });
//   }
// };

// const resetWorkerImgSlider = (event) => {
//   const workerImgs = event.target.querySelectorAll('.workers__img-wrap img');
//   for (let e = 0; e < workerImgs.length; e++) {
//     workerImgs[e].style.transform = 'translateX(0px)';
//   }
// };

// const handleWorkersListener = () => {
//   for (let i = 0; i < workers.length; i++) {
//     workers[i].addEventListener('mouseenter', (e) => {
//       workerImgSlider(e);
//     });
//     workers[i].addEventListener('mouseleave', (e) => {
//       resetWorkerImgSlider(e);
//     });
//   }
// };
