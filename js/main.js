let curPage = 1;
const numPages = document.getElementsByClassName('section').length;
const animTime = 1500;
const pgPrefix = 'section-';
let section;
let scrolling = false;
let scrollTimer = null;
const navDownArrow = document.getElementsByClassName('section__arrow');
const toggle = document.getElementById('toggle');
const navBar = document.getElementsByClassName('navigation');
const divider = document.querySelector('.divider');
const columns = document.querySelectorAll('.half-width__three-column');
const clients = document.querySelector('.section-5__clients');
const heartToggle = document.querySelectorAll('.heart-toggle');
const closeToggle = document.getElementById('close');
const videoEl = document.getElementById('video-wrap');
const videoIframe = videoEl && videoEl.querySelector('iframe');
const player = videoIframe && new Vimeo.Player(videoIframe);

heartToggle.forEach((heart) =>
  heart.addEventListener('click', () => {
    document.getElementById('full-screen').classList.add('active');
    player.play();
  })
);

closeToggle &&
  closeToggle.addEventListener('click', () => {
    document.getElementById('full-screen').classList.remove('active');
    player.pause();
  });

const handleMenuColor = () => {
  if (curPage > 1) {
    navBar[0].classList.add('color-me');
    toggle.classList.add('color-me');
  } else {
    navBar[0].classList.remove('color-me');
    toggle.classList.remove('color-me');
  }
};

const InitElInView = () => {
  let i;
  for (i = 1; i < numPages; i++) {
    let element = document.getElementById(pgPrefix + i);
    if (element !== null) {
      let elemRect = element.getBoundingClientRect();
      if (elemRect.top >= 0) {
        curPage = i;
        handleMenuColor();
        if (i !== 1) {
          element.classList.add('hide-me');
        }
        if (element.querySelector('.section__arrow')) {
          element.querySelector('.section__arrow').classList.add('show-me');
        }
        setTimeout(() => {
          element.classList.remove('hide-me');
          if (element.querySelector('.section__arrow')) {
            element
              .querySelector('.section__arrow')
              .classList.remove('show-me');
          }
        }, 400);
        return;
      }
    } else {
      curPage = 1;
    }
  }
};

InitElInView();

const pagination = () => {
  let section = document.getElementById(pgPrefix + curPage);
  section &&
    section.classList.contains('hide-me') &&
    section.classList.remove('hide-me');
  section &&
    section.classList.contains('hide-me-again') &&
    section.classList.remove('hide-me-again');
  divider &&
    divider.classList.contains('hide-me') &&
    divider.classList.remove('hide-me');
  divider &&
    divider.classList.contains('hide-me-again') &&
    divider.classList.remove('hide-me-again');
  columns &&
    columns.forEach((col) => {
      col.classList.contains('hide-me') && col.classList.remove('hide-me');
      col.classList.contains('hide-me-again') &&
        col.classList.remove('hide-me-again');
    });
  clients &&
    clients.classList.contains('hide-me') &&
    clients.classList.remove('hide-me');
  clients &&
    clients.classList.contains('hide-me-again') &&
    clients.classList.remove('hide-me-again');
  let sectionOffset = section && section.offsetTop;
  window.scroll({ top: sectionOffset, behavior: 'smooth' });
  handleMenuColor();
  if (section && section.querySelector('.section__arrow')) {
    section.querySelector('.section__arrow').classList.add('show-me');
  }
  setTimeout(() => {
    scrolling = false;
    // section.querySelector('.section__arrow').classList.remove('show-me');
  }, animTime);
};

const navigationUp = () => {
  if (curPage === 1) {
    setTimeout(() => {
      scrolling = false;
    }, animTime);
    return;
  }
  curPage--;
  pagination();
  curPage !== 1 &&
    document.getElementById(pgPrefix + curPage).classList.add('hide-me-again');
  curPage === 3 && divider.classList.add('hide-me-again');
  curPage === 4 &&
    columns.forEach((col) => {
      col.classList.add('hide-me-again');
    });
  curPage === 5 && clients.classList.add('hide-me-again');
};

const navigationDown = () => {
  if (curPage === numPages) {
    setTimeout(() => {
      scrolling = false;
    }, animTime);
    return;
  }
  curPage++;
  pagination();
  curPage !== 1 &&
    document.getElementById(pgPrefix + curPage).classList.add('hide-me');
  curPage === 3 && divider.classList.add('hide-me');
  curPage === 4 &&
    columns.forEach((col) => {
      col.classList.add('hide-me');
    });
  curPage === 5 && clients.classList.add('hide-me');
};

const sectionArrowClick = () => {
  for (let i = 0; i < navDownArrow.length; i++) {
    navDownArrow[i].addEventListener('click', () => {
      navigationDown();
    });
  }
};

sectionArrowClick();

const observer = new IntersectionObserver(
  (entries) => {
    if (entries[0].isIntersecting === true) {
      setTimeout(() => {
        entries[0].target.classList.remove('hide-me');
      }, 300);
    }
  },
  { root: document.getElementById('main'), threshold: [0.25] }
);

const wheelBehavior = (e) => {
  e.preventDefault();
  if (!scrolling) {
    scrolling = true;
    const scrollDirection = Math.sign(e.deltaY);
    if (scrollDirection > 0) {
      navigationDown();
    } else {
      navigationUp();
    }
  }
};

if (navigator.userAgent.indexOf('Firefox') !== -1) {
  window.addEventListener('scroll', wheelBehavior, false);
} else {
  if (window.location.pathname !== '/portfolio/') {
    document.addEventListener('wheel', wheelBehavior, { passive: false });
    document.addEventListener('touch', wheelBehavior, { passive: false });
  }
}

window.location.pathname === '/portfolio/' &&
  navBar[0].classList.add('color-me');

if (document.querySelector('.error404')) {
  navBar[0].classList.add('error-page');
}

const navLinks = document.querySelectorAll('nav ul li a');

const scrollClickHandler = (e) => {
  const regex = new RegExp('#');
  const href = e.target.attributes.href.value;
  const sectionNum = href.split('-')[1];
  curPage = sectionNum;
  handleMenuColor();

  // TODO add animation function here

  if (window.location.pathname === '/') {
    if (href.search(regex) !== -1) {
      e.preventDefault();
      const offsetTop = document.querySelector(href).offsetTop;

      window.scroll({
        top: offsetTop,
        behavior: 'smooth',
      });

      toggle.classList.toggle('active');
      menu.classList.toggle('visible');
    }
  } else {
    window.location.assign(`${window.location.origin + href}`);
  }
  curPage = href.split('-')[1];
};

for (const link of navLinks) {
  link.addEventListener('click', scrollClickHandler);
}
