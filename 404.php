<?php get_header(); ?>

<section class="section--error-section">
  <div class="row">
    <div class="full-width">
      <!-- <span id="arrow-404" class="arrow arrow__left">
        <a href="/">
          <?php // echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
        </a>
      </span> -->
      <h1>Please don&rsquo;t go.</h1>
      <p>We need to listen more.</p>
      <div class="play-toggle" onclick="document.getElementById('player').play()">
        <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
      </div>
    </div>
    <audio id="player" autoplay="true">
      <source
        src="http://localhost:10018/wp-content/uploads/2022/08/If-You-Leave-Me-Now-Chicago-Lyrics-audio-extractor.net_.mp3"
        type="audio/mpeg">
    </audio>
  </div>
  <div class="heart-toggle">
    <?php echo file_get_contents(get_stylesheet_directory_uri() . "/img/heart.svg"); ?>
  </div>
</section>

<div id="full-screen" class="full-screen">
  <div id="close" class="full-screen__close">
    <span></span>
  </div>
  <div id="video-wrap" class="full-screen__video" style="padding:56.25% 0 0 0;position:relative;">
    <iframe src="https://player.vimeo.com/video/717465549?h=df8836fc45&loop=1&autopause=0&player_id=0&app_id=58479"
      style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0"
      allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
  </div>
  <script src="https://player.vimeo.com/api/player.js"></script>
</div>

<?php get_footer(); ?>