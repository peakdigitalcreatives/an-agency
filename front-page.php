<?php get_header(); ?>
<?php // $postNumber = 58; ?>
<?php $postNumber = 109; ?>

<main id="main">
  <section id="section-1" class="section section-1">
    <div class="row">
      <div class="half-width">
        <div class="section-1__video" style="padding:56.25% 0 0 0;position:relative;">

          <iframe
            src="https://player.vimeo.com/video/738315170?h=3acbbd5858&badge=0&background=1&autoplay=1&loop=1&title=0&byline=0&portrait=0"
            style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0"
            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>


          <!-- <iframe
            src="https://player.vimeo.com/video/717464594?h=754a9d28b8&badge=0&background=1&autoplay=1&loop=1&autopause=0&player_id=0&app_id=58479&muted=1"
            style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0"
            allow="autoplay; fullscreen; picture-in-picture" scrolling="no" allowfullscreen></iframe> -->
        </div>
        <script src="https://player.vimeo.com/api/player.js"></script>
      </div>
    </div>
    <div class="heart-toggle">
      <?php echo file_get_contents(get_stylesheet_directory_uri() . "/img/heart.svg"); ?>
    </div>
    <span class="section__arrow section__arrow--bottom">
      <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
    </span>
  </section>
  <section id="section-2" class="section section-2">
    <div class="row row__column">
      <div class="full-width">
        <h1><?php the_field('relationship_header', $postNumber); ?></h1>
      </div>
      <div class="half-width half-width__column">
        <p><?php the_field('relationship_content', $postNumber); ?></p>
        <span class="section__arrow">
          <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
        </span>
      </div>
    </div>
  </section>
  <section id="section-3" class="section section-3">
    <div class="row">
      <div id="overflow-section-wrap" class="overflow-section-wrap">
        <div id="overflow-section" class="overflow-section">

          <?php include(TEMPLATEPATH . '/templates/thework.php'); ?>

          <div id="nav-section" class="full-width nav-section">
            <div class="half-width">
              <span id="arrow-left" class="arrow arrow__left">
                <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
              </span>
              <span id="work-svg">
                <?php echo file_get_contents(get_template_directory() . '/img/work.svg') ?>
              </span>
            </div>
            <div class="divider"></div>
            <div class="half-width">
              <span id="workers-svg">
                <?php echo file_get_contents(get_template_directory() . '/img/workers.svg') ?>
              </span>
              <span id="arrow-right" class="arrow arrow__right">
                <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
              </span>
            </div>
          </div>

          <?php include(TEMPLATEPATH . '/templates/workers.php'); ?>

          <span class="section__arrow section__arrow--bottom">
            <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
          </span>

        </div>
      </div>
  </section>
  <section id="section-4" class="section section-4">
    <div class="row row__column">
      <div class="full-width full-width__column">
        <h2><?php the_field('capabilities_header', $postNumber); ?></h2>
      </div>
      <div class="half-width half-width__content">
        <p><?php the_field('capabilities_content', $postNumber); ?></p>
      </div>
      <div class="three-quarters-width">
        <div class="half-width__three-column">
          <div>
            <h3><?php the_field('section_1_header', $postNumber); ?></h3>
            <p><?php the_field('section_1_content', $postNumber); ?></p>
          </div>
          <div>
            <h3><?php the_field('section_2_header', $postNumber); ?></h3>
            <p><?php the_field('section_2_content', $postNumber); ?></p>
          </div>
        </div>
        <div class="half-width__three-column">
          <div>
            <h3><?php the_field('section_3_header', $postNumber); ?></h3>
            <p><?php the_field('section_3_content', $postNumber); ?></p>
          </div>
          <div>
            <h3><?php the_field('section_4_header', $postNumber); ?></h3>
            <p><?php the_field('section_4_content', $postNumber); ?></p>
          </div>
        </div>
        <div class="half-width__three-column">
          <div>
            <h3><?php the_field('section_5_header', $postNumber); ?></h3>
            <p><?php the_field('section_5_content', $postNumber); ?></p>
          </div>
          <div>
            <h3><?php the_field('section_6_header', $postNumber); ?></h3>
            <p><?php the_field('section_6_content', $postNumber); ?></p>
          </div>
        </div>
      </div>
      <span class="section__arrow section__arrow--bottom">
        <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
      </span>
    </div>
  </section>
  <section id="section-5" class="section section-5">
    <div class="row row__column">
      <div class="full-width full-width__column">
        <h2><?php the_field('clients_header', $postNumber); ?></h2>
      </div>
      <div class="half-width half-width__column">
        <p><?php the_field('clients_content', $postNumber); ?></p>
      </div>
      <div class="section-5__clients">
        <?php if (have_rows('clients', $postNumber)):
             while (have_rows('clients', $postNumber)) : the_row(); ?>
        <div class="image-wrap">
          <img loading="lazy" src="<?php the_sub_field('logo'); ?>" alt="" />
        </div>
        <?php endwhile;
            endif; ?>
      </div>
      <span class="section__arrow section__arrow--bottom">
        <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
      </span>
  </section>
  <section id="section-6" class="section section-6">
    <div class="row row__column">
      <div class="full-width full-width__column">
        <h2><?php the_field('contact_header', $postNumber); ?></h2>
      </div>
      <div class="half-width half-width__column">
        <p><?php the_field('contact_content', $postNumber); ?></p>
      </div>
      <div class="full-width full-width__column">
        <form action="">
          <input type="text" placeholder="Name">
          <input type="text" placeholder="Email">
          <div class="two-col">
            <div>
              <textarea type="text" rows="4" placeholder="Message"></textarea>
            </div>
            <div>
              <button type="submit" class="submit">
                <?php echo file_get_contents(get_template_directory() . "/img/submit-arrow.svg"); ?>
                <!-- <i class="fa fa-arrow-right" aria-hidden="true"></i> -->
              </button>
            </div>
          </div>
        </form>
        <div class="contact">
          <p>900 East Jefferson Street // Louisville, KY 40206 // 502.373.2328 // <a
              href="mailto:reception@anagency.com">reception@anagency.com</a></p>
        </div>
      </div>
    </div>
  </section>
</main>

<div id="full-screen" class="full-screen">
  <div id="close" class="full-screen__close">
    <span></span>
  </div>
  <div id="video-wrap" class="full-screen__video" style="padding:56.25% 0 0 0;position:relative;">
    <iframe src="https://player.vimeo.com/video/717465549?h=df8836fc45&loop=1&autopause=0&player_id=0&app_id=58479"
      style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0"
      allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
  </div>
  <script src="https://player.vimeo.com/api/player.js"></script>
</div>
<?php get_footer(); ?>