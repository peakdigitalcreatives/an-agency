<footer id="section-7" class="section footer">
  <div class="bg-rules">
    <img src="<?php bloginfo('template_directory'); ?>/img/bottom-rules.png" alt="">
  </div>
  <div class="row">
    <div class="full-width">
      <div>
        &copy;
        <script type="text/javascript">
        document.write((new Date().getFullYear()).toString());
        </script>
        An Agency Advertising
      </div>
      <div>
        <ul>
          <li>
            <a href="/">
              Home
            </a>
          </li>
          <li>
            <a href="/portfolio">
              Portfolio
            </a>
          </li>
          <li>
            <a href="#workers">
              Team
            </a>
          </li>
          <li>
            <a href="#section-6">
              Contact
            </a>
          </li>
          <li>
            <a href="#">
              Privacy Policy
            </a>
          </li>
        </ul>
      </div>
      <div>
        <ul>
          <li>
            <a target="_blank" href="https://www.facebook.com/anagencyadvertising">
              Facebook
            </a>
          </li>
          <li>
            <a target="_blank" href="https://www.instagram.com/anagencyadvertising">
              Instagram
            </a>
          </li>
          <li>
            <a target="_blank" href="https://www.linkedin.com/company/an-agency-advertising">
              LinkedIn
            </a>
          </li>
          <li>
            <a target="_blank" href="https://vimeo.com/anagency">
              Vimeo
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
</body>

</html>