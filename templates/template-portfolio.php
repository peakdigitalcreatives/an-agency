<?php /* Template Name: Portfolio */ get_header(); ?>

<div id="transition"></div>
<section class="section section--posts">

  <div class="row row__column">
    <div class="half-width">
      <h2>Getting emotional</h2>
    </div>
    <div class="half-width half-width__column">
      <p>If you’re proud of why your brand is different. If you believe that what you possess can change the world in
        positive ways. If you have the grit and spit to take on all comers. If you’re proud of why your brand is
        different. If you believe that what you possess can change the world in positive ways. If you have the grit and
        spit to take on all comers.</p>
    </div>
    <div class="full-width full-width--post-list">

      <?php 
					$i = 0; 
					$args = array(
						'post_type'				=> 'post',
						'posts_per_page'	=> -1
					);
					$query = new WP_Query( $args ); 
					if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>

      <div <?php post_class('portfolio-toggle') ?> id="post-<?php the_ID(); ?>"
        data-portfolio="<?php echo the_title(); ?>" style="background-image: url('<?php if ( has_post_thumbnail() ) {
                  the_post_thumbnail_url();
              }; ?>')">
        <?php if(the_field('featured_video')) {
            echo '<div class="bg-image">';
            // the_field('featured_video');
            echo '</div>';
          } ?>
      </div>
      <div id="<?php echo the_title(); ?>" class="work__popup__wrap">
        <div id="popup-content-wrap-<?php echo $i; ?>" class="work__popup__content-wrap">
          <div class="close-portfolio">
            <span></span>
          </div>
          <div id="post-popup-content-<?php echo $i; ?>" class="work__popup__content">
            <?php the_content(); ?>
          </div>
        </div>
      </div>

      <?php $i++; ?>

      <?php endwhile; ?>

      <?php else : ?>

      <h2>Not Found</h2>

      <?php endif; ?>

    </div>

  </div>

</section>

<?php get_footer(); ?>