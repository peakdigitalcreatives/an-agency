<?php ?>

<div id="workers" class="full-width full-width__column workers">
  <span id="workers-arrow-back" class="arrow">
    <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
  </span>
  <!-- <div id="workers-content" class="half-width half-width__column workers__section">
                <h2>Who we are. Who we aren't</h2>
                <p>If you&rsquo;re proud of why your brand is different. If you believe that what you possess can change the world in positive ways. If you have the grit and spit to take on all comers. And if you hold that values like courage, passion, loyalty, commitment, purpose, intelligence, resolve, and hard work define a creative advertising relationship, then we believe in the same things.</p>
              </div> -->
  <div id="workers-images" class="workers__section">
    <div class="workers__row">
      <?php if (have_rows('workers', $postNumber)):
                    while (have_rows('workers', $postNumber)) : the_row(); ?>
      <div class="workers__worker">
        <div class="workers__img-frame">
          <img src="<?php bloginfo('template_directory'); ?>/img/<?php echo the_sub_field('border'); ?>.png" />
          <span id="prev" class="nav-links nav-links--prev">
            <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
          </span>
          <span id="next" class="nav-links nav-links--next">
            <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
          </span>
        </div>
        <div class="workers__img-wrap">
          <?php $count = 1; 
            if (have_rows('images')):
                        while (have_rows('images')) : the_row(); ?>
          <img loading="lazy" src="<?php the_sub_field('image'); ?>" alt=""
            class="<?php if($count === 1) {echo 'active-img';}; ?>" />
          <?php $count++;
          endwhile;
                      endif; ?>
        </div>
        <div class="workers__text">
          <p class="workers__name"><a
              href="mailto: <?php the_sub_field('email_address'); ?>"><?php the_sub_field('name'); ?></a></p>
          <p class="workers__title"><?php the_sub_field('position'); ?></p>
        </div>
      </div>
      <?php endwhile;
                  endif; ?>
    </div>
  </div>
</div>

<?php ?>