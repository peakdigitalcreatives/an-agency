<?php ?>

<div id="work" class="full-width work">
  <!-- <span id="work-arrow-back" class="arrow">
              <?php // echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
            </span> -->
  <div id="transition"></div>
  <div class="rules rules--top">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <div class="rules rules--bottom">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
  <?php 
            $i = 1;
            $args = array(
              'post_type'				=> 'post',
              'posts_per_page'	=> -1,
              'post__in'        => get_option( 'sticky_posts' ),
            );
            $query = new WP_Query( $args );
            if($query->have_posts()) : 

              while($query->have_posts()) : $query->the_post() ;?>

  <div id="work-<?php echo $i; ?>" class="work__section" style="background-image: url(<?php if ( has_post_thumbnail() ) {
                  the_post_thumbnail_url();
              }; ?>); z-index: <?php echo $i; ?>">
    <div class="bg-image">
      <?php if(the_field('featured_video')) { ?>
      <?php the_field('featured_video'); ?>
      <?php } ?>
    </div>
    <div class="row">
      <div class="full-width">
        <div class="work__work-toggle work-toggle <?php if($i > 1) { echo 'animate';}; ?>"
          data-work="<?php the_title(); ?>">
          <?php the_title(); ?>
        </div>
      </div>
    </div>
    <div id="<?php the_title(); ?>" class="work__popup__wrap">
      <div id="popup-content-wrap-<?php echo $i; ?>" class="work__popup__content-wrap">
        <div class="close-work">
          <span></span>
          <span></span>
        </div>
        <div id="popup-content-<?php echo $i; ?>" class="work__popup__content">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </div>


  <?php $i++ ;?>
  <?php wp_reset_postdata();
                endwhile;
              endif; ?>
  <div class="work__bottom-wrap">
    <div class="work__bottom">
      <span id="work-arrow-back" class="arrow">
        <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
      </span>
      <span class="work__bottom__arrow">
        <?php echo file_get_contents(get_template_directory() . "/img/arrow.svg"); ?>
      </span>
    </div>
  </div>
</div>

<?php ?>