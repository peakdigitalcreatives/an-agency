<?php /* Template Name: Workers */ get_header();?>
<?php $postNumber = 109; ?>

<section class="section">

  <div class="row row__column">

    <div class="full-width">

      <?php include(TEMPLATEPATH . '/templates/workers.php'); ?>

    </div>

  </div>

</section>

<?php get_footer(); ?>