<?php

	// =========================================================================
	// REGISTER AND LOAD ALL STYLESHEETS AND SCRIPTS FOR THE THEME
	// =========================================================================
	function startTheme(){
		wp_enqueue_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
		wp_enqueue_style('slick-style', ('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css'));
		wp_enqueue_style('theme-style', get_template_directory_uri() . '/css/main.css');
		wp_deregister_script('jquery');
		wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"), '', '2.2.2', true);
		wp_enqueue_script('jquery');
		wp_register_script('slick-script', ('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js'), '', '1.11.3', true);
		wp_enqueue_script('slick-script');
		wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/main.min.js', '', '1.0', true );
		wp_enqueue_script('menu-nav-js', get_template_directory_uri() . '/js/menu.min.js', '', '1.0', true );
		wp_enqueue_script('work-workers-js', get_template_directory_uri() . '/js/workWorkers.min.js', '', '1.0', true );
	}
	add_action('wp_enqueue_scripts', 'startTheme');

	// =========================================================================
	// ADD GOOGLE ANALYTICS TO THE FOOTER
	// =========================================================================
	// function add_google_analytics() {
	// 	echo '<script src="https://www.google-analytics.com/ga.js" type="text/javascript"></script>';
	// 	echo '<script type="text/javascript">';
	// 	echo 'var pageTracker = _gat._getTracker("UA-XXXXX-X");';
	// 	echo 'pageTracker._trackPageview();';
	// 	echo '</script>';
	// }
	// add_action('wp_footer', 'add_google_analytics');

	// =========================================================================
	// CUSTOM EXCERPT LENGTH
	// =========================================================================
	// function custom_excerpt_length($length) {
	// 	return 20;
	// }
	// add_filter('excerpt_length', 'custom_excerpt_length');

	// =========================================================================
	// ADD RSS LINKS TO HEAD SECTION
	// =========================================================================
	add_theme_support('automatic-feed-links');

	// =========================================================================
	// REMOVE EMOJI JS AND JUNK
	// =========================================================================
	// function disable_wp_emojicons() {

	//   // all actions related to emojis
	//   remove_action( 'admin_print_styles', 'print_emoji_styles' );
	//   remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	//   remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	//   remove_action( 'wp_print_styles', 'print_emoji_styles' );
	//   remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	//   remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	//   remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	//   // filter to remove TinyMCE emojis
	//   add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
	// }
	// add_action( 'init', 'disable_wp_emojicons' );

	// =========================================================================
	// REMOVE JUNK FROM HEAD AND STUFF
	// =========================================================================
	remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
	remove_action('wp_head', 'wp_generator'); // remove wordpress version

	remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
	remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links

	remove_action('wp_head', 'index_rel_link'); // remove link to index page
	remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)

	remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
	remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );

	// =========================================================================
	// REGISTERING SIDEBAR
	// =========================================================================
	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name' => 'Sidebar Widgets',
			'id'   => 'sidebar-widgets',
			'description'   => 'These are widgets for the sidebar.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>'
		));
	}

	// =========================================================================
	// HIDE ADMIN BAR ON FRONTEND
	// =========================================================================
	// This is not recommended by default
	add_filter('show_admin_bar', '__return_false');

	// =========================================================================
	// ENABLES FEATURED IMAGES FOR PAGES AND POSTS
	// =========================================================================
	// This enables post thumbnails for all post types,
	// if you want to enable this feature for specific post types,
	// use the array to include the type of post
	// add_theme_support('post-thumbnails', array('post', 'page'));
	add_theme_support('post-thumbnails');

	// =========================================================================
	// ENABLES 100% JPEG QUALITY
	// =========================================================================
	// Wordpress will compress uploads to 90% of their original size
	add_filter( 'jpg_quality', 'high_jpg_quality' );
		function high_jpg_quality() {
		return 100;
	}

	// =========================================================================
	// TITLE TAG - RECOMMENDED
	// =========================================================================
	// Since Version 4.1, themes should use add_theme_support() in the functions.php
	// file in order to support title tag
	function theme_slug_setup() {
	   add_theme_support( 'title-tag' );
	}
	add_action( 'after_setup_theme', 'theme_slug_setup' );

	// =========================================================================
	// WORDPRESS CONTENT WIDTH - REQUIRED
	// =========================================================================
	// Since Version 2.6, themes need to specify the $content_width variable

	// Using this feature you can set the maximum allowed width for any content
	// in the theme, like oEmbeds and images added to posts.

	// Using this theme feature, WordPress can scale oEmbed code to a specific
	// size (width) in the front-end, and insert large images without breaking the
	// main content area. Also, using this feature you lay the ground for other
	// plugins to perfectly integrate with any theme, since plugins can access the
	// value stored in $content_width.
	if ( ! isset( $content_width ) ) {
		$content_width = 600;
	}


	// =========================================================================
	// REMOVING COMMENTS ALL TOGETHER
	// =========================================================================
	// Removing the default comment system on Wordpress
	function disable_comments_post_types_support() {
		$post_types = get_post_types();
		foreach ($post_types as $post_type) {
			if(post_type_supports($post_type, 'comments')) {
				remove_post_type_support($post_type, 'comments');
				remove_post_type_support($post_type, 'trackbacks');
			}
		}
	}
	add_action('admin_init', 'disable_comments_post_types_support');

	// Close comments on the front-end
	function disable_comments_status() {
		return false;
	}
	add_filter('comments_open', 'disable_comments_status', 20, 2);
	add_filter('pings_open', 'disable_comments_status', 20, 2);

	// Hide existing comments
	function disable_comments_hide_existing_comments($comments) {
		$comments = array();
		return $comments;
	}
	add_filter('comments_array', 'disable_comments_hide_existing_comments', 10, 2);

	// Remove comments page in menu
	function disable_comments_admin_menu() {
		remove_menu_page('edit-comments.php');
	}
	add_action('admin_menu', 'disable_comments_admin_menu');

	// Redirect any user trying to access comments page
	function disable_comments_admin_menu_redirect() {
		global $pagenow;
		if ($pagenow === 'edit-comments.php') {
			wp_redirect(admin_url()); exit;
		}
	}
	add_action('admin_init', 'disable_comments_admin_menu_redirect');

	// Remove comments metabox from dashboard
	function disable_comments_dashboard() {
		remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
	}
	add_action('admin_init', 'disable_comments_dashboard');

	// Remove comments links from admin bar
	function disable_comments_admin_bar() {
		if (is_admin_bar_showing()) {
			remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
		}
	}
	add_action('init', 'disable_comments_admin_bar');


	// =========================================================================
	// ADD NEW IMAGE SIZES WHEN UPLOADING MEDIA
	// =========================================================================
	// Name the type of size, provide the demensions, and determine
	// how the image should be cropped, if cropped at all
	// add_image_size( 'cropped-blog', 370, 220, array( 'center', 'center' ) );

	// =========================================================================
	// DEREGISTER CONTACT FORM 7 STYLES AND JS
	// =========================================================================
	// add_action( 'wp_print_styles', 'aa_deregister_styles', 100 );
	// function aa_deregister_styles() {
	// 	if ( ! is_page( 'contact-us' ) ) {
	// 		wp_deregister_style( 'contact-form-7' );
	// 	}
	// }

	// add_action( 'wp_print_scripts', 'aa_deregister_javascript', 100 );
	// function aa_deregister_javascript() {
	// 	if ( ! is_page( 'contact-us' ) ) {
	// 		wp_deregister_script( 'contact-form-7' );
	// 	}
	// }

	// =========================================================================
	// REGISTER MENUS
	// =========================================================================
	// If more than one menu is neccessary, you can register several
	// and assign different menu items to each or use one menu for
	// desktop and another for mobile
	register_nav_menus( array(
		'main_menu' => 'Main Menu',
		'mobile_menu' => 'Mobile Menu'
	) );

	// =========================================================================
	// REGISTER A NEW CUSTOM POST TYPE
	// =========================================================================

	// Create a custom taxonomy
	// add_action( 'init', 'create_customPost_category', 0 );
	//
	// function create_customPost_category() {
	// 	$labels = array(
	// 		'name'              => _x( 'Post Categories', 'taxonomy general name' ),
	// 		'singular_name'     => _x( 'Post Category', 'taxonomy singular name' ),
	// 		'search_items'      => __( 'Search Post Category' ),
	// 		'all_items'         => __( 'All Post Categories' ),
	// 		'parent_item'       => __( 'Parent Post Category' ),
	// 		'parent_item_colon' => __( 'Parent Post Category:' ),
	// 		'edit_item'         => __( 'Edit Post Category' ),
	// 		'update_item'       => __( 'Update Post Category' ),
	// 		'add_new_item'      => __( 'Add New Post Category' ),
	// 		'new_item_name'     => __( 'New Post Category Name' ),
	// 		'menu_name'         => __( 'Post Category' ),
	// 	);
	//
	// 	$args = array(
	// 		'hierarchical'      => true,
	// 		'labels'            => $labels,
	// 		'show_ui'           => true,
	// 		'show_admin_column' => true,
	// 		'query_var'         => true,
	// 		'rewrite'           => array( 'slug' => 'post-category' ),
	// 	);
	//
	// 	register_taxonomy( 'post_category', array( 'post' ), $args );
	// }

	// Create custom post type
	// add_action( 'init', 'custom_post' );
	//
	// function custom_post() {
	// 	$labels = array(
	// 		'name'               => _x( 'Custom Posts', 'post type general name' ),
	// 		'singular_name'      => _x( 'Custom Post', 'post type singular name' ),
	// 		'menu_name'          => _x( 'Custom Posts', 'admin menu' ),
	// 		'name_admin_bar'     => _x( 'Custom Post', 'add new on admin bar' ),
	// 		'add_new'            => _x( 'Add New', 'custom post' ),
	// 		'add_new_item'       => __( 'Add New Custom Posts' ),
	// 		'new_item'           => __( 'New Custom Post' ),
	// 		'edit_item'          => __( 'Edit Custom Post' ),
	// 		'view_item'          => __( 'View Custom Post' ),
	// 		'all_items'          => __( 'All Custom Posts' ),
	// 		'search_items'       => __( 'Search Custom Posts' ),
	// 		'parent_item_colon'  => __( 'Parent Custom Posts:' ),
	// 		'not_found'          => __( 'No Custom Posts found.' ),
	// 		'not_found_in_trash' => __( 'No Custom Posts found in Trash.')
	// 	);
	//
	// 	$args = array(
	// 		'labels'             => $labels,
	// 		'public'             => true,
	// 		'publicly_queryable' => true,
	// 		'show_ui'            => true,
	// 		'show_in_menu'       => true,
	// 		'query_var'          => true,
	// 		'rewrite'            => array( 'slug' => 'custom-post' ),
	// 		'capability_type'    => 'post',
	// 		'has_archive'        => true,
	// 		'hierarchical'       => false,
	// 		'menu_position'      => null,
	// 		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	// 	);
	//
	// 	register_post_type( 'custom_post', $args );
	// }

?>