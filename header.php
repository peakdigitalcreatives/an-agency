<!doctype html>

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

  <head>

    <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <?php if (is_search()) { ?>
    <meta name="robots" content="noindex, nofollow">
    <?php } ?>

    <title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '—', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " — $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' — ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );

	?></title>

    <!-- fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700&display=swap" rel="stylesheet">

    <!-- stylesheets -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />

    <!-- favicon -->
    <link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.ico"
      type="image/x-icon" />

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

    <!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

    <?php wp_head(); ?>

  </head>

  <body <?php body_class(); ?>>

    <div class="navigation">
      <div class="logo">
        <a href="/">
          <?php echo file_get_contents(get_stylesheet_directory_uri() . "/img/An-Agency-Logo.svg"); ?>
        </a>
      </div>
      <div onclick="toggleSwitch" id="toggle"></div>
      <nav id="nav">
        <div class="main-menu">
          <?php wp_nav_menu( array('theme_location' => 'main_menu') ); ?>
        </div>
        <div class="mobile-menu">
          <?php wp_nav_menu( array('theme_location' => 'mobile_menu') ); ?>
        </div>
        <!-- <ul>
          <li>
            <a href="#section-2">Relationships</a>
          </li>
          <li>
            <a href="#section-3">The Work</a>
          </li>
          <li>
            <a href="/portfolio">Even More Work</a>
          </li>
          <li>
            <a href="#section-3">Workers</a>
          </li>
          <li>
            <a href="#section-4">Capabilities</a>
          </li>
          <li>
            <a href="#section-5">Clients</a>
          </li>
          <li>
            <a href="#section-6">Contact</a>
          </li>
        </ul> -->
        <div class="heart-toggle">
          <?php echo file_get_contents(get_stylesheet_directory_uri() . "/img/heart.svg"); ?>
        </div>
      </nav>
    </div>


    <?php //wp_nav_menu( $args ); ?>